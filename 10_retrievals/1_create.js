const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  "claim-id": "200002020654",
  retrievalRequestReason: "6343",
  docNeeded: "2"
};
mastercom.Retrievals.create(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.requestId); //-->300002296235
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
