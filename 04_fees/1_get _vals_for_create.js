const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  "claim-id": "200002020654",
  reasonCode: "4853"
};
mastercom.Fees.getPossibleValueListsForCreate(requestData, function(
  error,
  data
) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.currencies[0].name); //-->USD
    out(data.currencies[0].value); //-->USD
    out(data.reasonCodes[0].name); //-->7604
    out(data.reasonCodes[0].value); //-->7604 - Emergency card replacement fee
    out(data.countryCodes[0].name); //-->UNITED STATES
    out(data.countryCodes[0].value); //-->UNITED STATES
    out(data.messageTexts[0].name); //-->LOST/STOLEN CARD TRANSACTION FEE
    out(data.messageTexts[0].value); //-->LOST/STOLEN CARD TRANSACTION FEE
    //This sample shows looping through currencies
    console.log("This sample shows looping through currencies");
    data.currencies.forEach(function(item) {
      outObj(item, "name");
      outObj(item, "value");
    });
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
