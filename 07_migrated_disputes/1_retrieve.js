const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  pageSize: "4",
  pageNumber: "2",
  activityType: "CHARGEBACK",
  ica: "2222123456"
};
mastercom.MigratedDisputes.retrieve(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.migrationResponseList[0].claimId); //-->200000001235
    out(data.migrationResponseList[0].itemId); //-->300000002345
    out(data.migrationResponseList[0].itemType); //-->CB
    out(data.migrationResponseList[0].mastercomId); //-->12345
    out(data.migrationResponseList[0].acquirerReferenceData); //-->51234567890123456789012
    out(data.migrationResponseList[0].issuerReferenceData); //-->1234567890
    out(data.migrationResponseList[0].transactionAmount); //-->000000022893
    out(data.migrationResponseList[0].banknetReferenceNumber); //-->MCC0000010124
    out(data.migrationResponseList[1].claimId); //-->200000001236
    out(data.migrationResponseList[1].itemId); //-->300000002346
    out(data.migrationResponseList[1].itemType); //-->CB
    out(data.migrationResponseList[1].mastercomId); //-->234567
    out(data.migrationResponseList[1].acquirerReferenceData); //-->51234567890123456789123
    out(data.migrationResponseList[1].issuerReferenceData); //-->1234567891
    out(data.migrationResponseList[1].transactionAmount); //-->000000022894
    out(data.migrationResponseList[1].banknetReferenceNumber); //-->MCC0000010125
    out(data.migrationResponseList[2].claimId); //-->200000001237
    out(data.migrationResponseList[2].itemId); //-->300000002347
    out(data.migrationResponseList[2].itemType); //-->CB
    out(data.migrationResponseList[2].mastercomId); //-->345678
    out(data.migrationResponseList[2].acquirerReferenceData); //-->51234567890123456789321
    out(data.migrationResponseList[2].issuerReferenceData); //-->1234567892
    out(data.migrationResponseList[2].transactionAmount); //-->000000022895
    out(data.migrationResponseList[2].banknetReferenceNumber); //-->MCC0000010126
    out(data.migrationResponseList[3].claimId); //-->200000001238
    out(data.migrationResponseList[3].itemId); //-->300000002348
    out(data.migrationResponseList[3].itemType); //-->CB
    out(data.migrationResponseList[3].mastercomId); //-->456789
    out(data.migrationResponseList[3].acquirerReferenceData); //-->51234567890123456789456
    out(data.migrationResponseList[3].issuerReferenceData); //-->1234567893
    out(data.migrationResponseList[3].transactionAmount); //-->000000022896
    out(data.migrationResponseList[3].banknetReferenceNumber); //-->MCC0000010127
    out(data.totalCount); //-->12
    //This sample shows looping through migrationResponseList
    console.log("This sample shows looping through migrationResponseList");
    data.migrationResponseList.forEach(function(item) {
      outObj(item, "claimId");
      outObj(item, "itemId");
      outObj(item, "itemType");
      outObj(item, "mastercomId");
      outObj(item, "acquirerReferenceData");
      outObj(item, "issuerReferenceData");
      outObj(item, "transactionAmount");
      outObj(item, "banknetReferenceNumber");
    });
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
