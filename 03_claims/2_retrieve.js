const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  "claim-id": "200002020654"
};
mastercom.Claims.retrieve("", requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.acquirerId); //-->002222
    out(data.acquirerRefNum); //-->55306608112341123451234
    out(data.primaryAccountNum); //-->52751494691484000
    out(data.claimId); //-->200002000191
    out(data.claimType); //-->Standard
    out(data.claimValue); //-->123.28 USD
    out(data.clearingDueDate); //-->2017-12-14
    out(data.clearingNetwork); //-->GCMS
    out(data.createDate); //-->2017-10-27
    out(data.dueDate); //-->2017-12-11
    out(data.transactionId); //-->g1f4kmlMcfQaLHtRX+WWB2TfOiyAIO0ZrxZ2zJ226sQuH6EsoypShLUwzD95i2QeIxoHYh7qrAqy9qMdbmDgw&#x3D;&#x3D;
    out(data.isAccurate); //-->true
    out(data.isAcquirer); //-->true
    out(data.isIssuer); //-->false
    out(data.isOpen); //-->true
    out(data.issuerId); //-->001111
    out(data.lastModifiedBy); //-->user1234
    out(data.lastModifiedDate); //-->2017-11-13
    out(data.merchantId); //-->0024038000200
    out(data.queueName); //-->Pending
    out(data.retrievalDetails.acquirerRefNum); //-->55306608112341123451234
    out(data.retrievalDetails.acquirerResponseCd); //-->D
    out(data.retrievalDetails.acquirerMemo); //-->This is an example memo .
    out(data.retrievalDetails.acquirerResponseDt); //-->2018-01-29
    out(data.retrievalDetails.amount); //-->196.42
    out(data.retrievalDetails.currency); //-->PLN
    out(data.retrievalDetails.claimId); //-->200002000191
    out(data.retrievalDetails.issuerResponseCd); //-->REJECT_DOCUMENTATION_NOT_AS_REQUIRED
    out(data.retrievalDetails.issuerRejectRsnCd); //-->02
    out(data.retrievalDetails.issuerMemo); //-->This is an example memo .
    out(data.retrievalDetails.issuerResponseDt); //-->2018-01-29
    out(data.retrievalDetails.imageReviewDecision); //-->I
    out(data.retrievalDetails.imageReviewDt); //-->2018-01-29
    out(data.retrievalDetails.primaryAcctNum); //-->52751494691484000
    out(data.retrievalDetails.requestId); //-->200002000151
    out(data.retrievalDetails.retrievalRequestReason); //-->6305
    out(data.retrievalDetails.docNeeded); //-->1
    out(data.retrievalDetails.createDate); //-->2017-10-30
    out(data.retrievalDetails.chargebackRefNum); //-->2000000000
    out(data.chargebackDetails[0].currency); //-->USD
    out(data.chargebackDetails[0].documentIndicator); //-->false
    out(data.chargebackDetails[0].messageText); //-->AUTHORIZATION DECLINED MMDDYY
    out(data.chargebackDetails[0].amount); //-->196.43
    out(data.chargebackDetails[0].reasonCode); //-->4808
    out(data.chargebackDetails[0].isPartialChargeback); //-->false
    out(data.chargebackDetails[0].chargebackType); //-->CHARGEBACK
    out(data.chargebackDetails[0].chargebackId); //-->200002000151
    out(data.chargebackDetails[0].claimId); //-->200002000191
    out(data.chargebackDetails[0].reversed); //-->false
    out(data.chargebackDetails[0].reversal); //-->false
    out(data.chargebackDetails[0].createDate); //-->2017-10-27
    out(data.chargebackDetails[0].chargebackRefNum); //-->2000000000
    out(data.chargebackDetails[1].currency); //-->USD
    out(data.chargebackDetails[1].documentIndicator); //-->false
    out(data.chargebackDetails[1].amount); //-->196.43
    out(data.chargebackDetails[1].reasonCode); //-->2001
    out(data.chargebackDetails[1].isPartialChargeback); //-->false
    out(data.chargebackDetails[1].chargebackType); //-->SECOND_PRESENTMENT
    out(data.chargebackDetails[1].chargebackId); //-->200002000151
    out(data.chargebackDetails[1].claimId); //-->200002000191
    out(data.chargebackDetails[1].reversed); //-->false
    out(data.chargebackDetails[1].reversal); //-->false
    out(data.chargebackDetails[1].createDate); //-->2017-11-08
    out(data.chargebackDetails[1].chargebackRefNum); //-->2000000000
    out(data.chargebackDetails[2].currency); //-->USD
    out(data.chargebackDetails[2].documentIndicator); //-->false
    out(data.chargebackDetails[2].amount); //-->61.64
    out(data.chargebackDetails[2].reasonCode); //-->4807
    out(data.chargebackDetails[2].isPartialChargeback); //-->false
    out(data.chargebackDetails[2].chargebackType); //-->ARB_CHARGEBACK
    out(data.chargebackDetails[2].chargebackId); //-->200002000151
    out(data.chargebackDetails[2].claimId); //-->200002000191
    out(data.chargebackDetails[2].reversed); //-->false
    out(data.chargebackDetails[2].reversal); //-->false
    out(data.chargebackDetails[2].createDate); //-->2017-10-30
    out(data.chargebackDetails[2].chargebackRefNum); //-->9000000006
    out(data.feeDetails[0].cardAcceptorIdCode); //-->Test ID
    out(data.feeDetails[0].cardNumber); //-->52751494691484000
    out(data.feeDetails[0].countryCode); //-->USA
    out(data.feeDetails[0].currency); //-->USD
    out(data.feeDetails[0].feeDate); //-->2018-02-07
    out(data.feeDetails[0].destinationMember); //-->001527
    out(data.feeDetails[0].feeAmount); //-->0.24
    out(data.feeDetails[0].creditSender); //-->true
    out(data.feeDetails[0].creditReceiver); //-->false
    out(data.feeDetails[0].message); //-->00000013502000000135020626065946717713065946
    out(data.feeDetails[0].reason); //-->7623
    out(data.feeDetails[0].feeId); //-->300002002247
    out(data.caseFilingDetails.caseFilingStatus); //-->Closed
    out(data.caseFilingDetails.caseFilingDetails.claimId); //-->200002000151
    out(data.caseFilingDetails.caseFilingDetails.claimType); //-->CaseFiling
    out(data.caseFilingDetails.caseFilingDetails.caseId); //-->9000000012
    out(data.caseFilingDetails.caseFilingDetails.caseType); //-->4
    out(data.caseFilingDetails.caseFilingDetails.chargebackRefNum[0]); //-->1111423456,2222123456
    out(data.caseFilingDetails.caseFilingDetails.currencyCode); //-->USD
    out(data.caseFilingDetails.caseFilingDetails.customerFilingNumber); //-->5482
    out(data.caseFilingDetails.caseFilingDetails.creditDate); //-->2018-11-01
    out(data.caseFilingDetails.caseFilingDetails.chargebackDate); //-->2018-11-01
    out(data.caseFilingDetails.caseFilingDetails.reasonCode); //-->4853
    out(data.caseFilingDetails.caseFilingDetails.disputeAmount); //-->50.00
    out(data.caseFilingDetails.caseFilingDetails.filingAgaintstIca); //-->5482
    out(data.caseFilingDetails.caseFilingDetails.filingAs); //-->A
    out(data.caseFilingDetails.caseFilingDetails.filingIca); //-->6000
    out(data.caseFilingDetails.caseFilingDetails.merchantName); //-->test name
    out(data.caseFilingDetails.caseFilingDetails.primaryAccountNum); //-->52751494691484000
    out(data.caseFilingDetails.caseFilingDetails.violationCode); //-->D.2
    out(data.caseFilingDetails.caseFilingDetails.violationDate); //-->2017-12-18
    out(data.caseFilingDetails.caseFilingDetails.rulingDate); //-->2018-01-24
    out(data.caseFilingDetails.caseFilingDetails.rulingStatus); //-->Favor Sender
    out(data.caseFilingDetails.caseFilingRespHistory[0].memo); //-->Sample Memo
    out(data.caseFilingDetails.caseFilingRespHistory[0].action); //-->FAVOR SENDER
    out(data.caseFilingDetails.caseFilingRespHistory[0].responseDate); //-->2018-01-24
    out(data.caseFilingDetails.caseFilingRespHistory[1].memo); //-->Sample Memo
    out(data.caseFilingDetails.caseFilingRespHistory[1].action); //-->REBUT
    out(data.caseFilingDetails.caseFilingRespHistory[1].responseDate); //-->2017-12-18
    out(data.caseFilingDetails.caseFilingRespHistory[2].memo); //-->Sample Memo
    out(data.caseFilingDetails.caseFilingRespHistory[2].action); //-->ESCALATE
    out(data.caseFilingDetails.caseFilingRespHistory[2].responseDate); //-->2017-12-18
    out(data.caseFilingDetails.caseFilingRespHistory[3].memo); //-->Sample Memo
    out(data.caseFilingDetails.caseFilingRespHistory[3].action); //-->REJECT
    out(data.caseFilingDetails.caseFilingRespHistory[3].responseDate); //-->2017-12-18
    //This sample shows looping through chargebackDetails
    console.log("This sample shows looping through chargebackDetails");
    data.chargebackDetails.forEach(function(item) {
      outObj(item, "currency");
      outObj(item, "documentIndicator");
      outObj(item, "messageText");
      outObj(item, "amount");
      outObj(item, "reasonCode");
      outObj(item, "isPartialChargeback");
      outObj(item, "chargebackType");
      outObj(item, "chargebackId");
      outObj(item, "claimId");
      outObj(item, "reversed");
      outObj(item, "reversal");
      outObj(item, "createDate");
      outObj(item, "chargebackRefNum");
    });
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
