/**
 *
 * Script-Name: get_data_for_create_chargeback_sample
 */

var mastercom = require("mastercard-mastercom");
var MasterCardAPI = mastercom.MasterCardAPI;
var ResourceConfig = mastercom.ResourceConfig;

var consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000"; // You should copy this from "My Keys" on your project page e.g. UTfbhDCSeNYvJpLL5l028sWL9it739PYh6LU5lZja15xcRpY!fd209e6c579dc9d7be52da93d35ae6b6c167c174690b72fa
var keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12"; // e.g. /Users/yourname/project/sandbox.p12 | C:\Users\yourname\project\sandbox.p12
var keyAlias = "keyalias"; // For production: change this to the key alias you chose when you created your production key
var keyPassword = "keystorepassword"; // For production: change this to the key alias you chose when you created your production key

// You only need to do initialize MasterCardAPI once
//
var authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
// This is needed to change the environment to run the sample code. For production, remove the next line and pass sandbox: false in the init() function.
MasterCardAPI.setEnvironment("sandbox");

var requestData = {
  "claim-id": "200002020654",
  chargebackType: "CHARGEBACK",
  reasonCode: "4853"
};
mastercom.Chargebacks.getPossibleValueListsForCreate(requestData, function(
  error,
  data
) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.currencies[0].name); //-->USD
    out(data.currencies[0].value); //-->USD
    out(data.docIndicators[0].name); //-->1
    out(data.docIndicators[0].value); //-->1 - Supporting documentation will follow
    out(data.messageTexts[0].name); //-->CARD NOT VALID OR EXPIRED
    out(data.messageTexts[0].value); //-->CARD NOT VALID OR EXPIRED
    out(data.reasonCodes[0].name); //-->4831
    out(data.reasonCodes[0].value); //-->4831 - Transaction Amount Differs
    //This sample shows looping through currencies
    console.log("This sample shows looping through currencies");
    data.currencies.forEach(function(item) {
      outObj(item, "name");
      outObj(item, "value");
    });
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
