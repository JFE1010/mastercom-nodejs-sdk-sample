const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

const requestData = {
  // insert requestData here
  chargebackList: [
    {
      claimId: "12344",
      chargebackId: "12344"
    }
  ]
};

// process requestData here:
mastercom.Chargebacks.chargebacksStatus(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.chargebackResponseList[0].chargebackId); //-->12344
    out(data.chargebackResponseList[0].status); //-->COMPLETED
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
