const mastercom = require("mastercard-mastercom");
const MastercardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MastercardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MastercardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MastercardAPI.setEnvironment("sandbox");

const requestData = {
  "claim-id": "12344",
  currency: "USD",
  documentIndicator: "true",
  messageText: "test message",
  amount: "100.00",
  fileAttachment: {
    filename: "test.tif",
    file: "sample file"
  },
  reasonCode: "4853",
  chargebackType: "ARB_CHARGEBACK"
};
mastercom.Chargebacks.create(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.chargebackId);
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
