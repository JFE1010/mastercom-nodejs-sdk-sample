const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  "queue-name": "Closed"
};
mastercom.Queues.retrieveClaimsFromQueue(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data[0].acquirerId); //-->00000005195
    out(data[0].acquirerRefNum); //-->05103246259000000000126
    out(data[0].primaryAccountNum); //-->52751494691484000
    out(data[0].claimId); //-->200002020654
    out(data[0].clearingDueDate); //-->2017-11-13
    out(data[0].clearingNetwork); //-->GCMS
    out(data[0].createDate); //-->2017-11-13
    out(data[0].dueDate); //-->2017-11-13
    out(data[0].transactionId); //-->118411681
    out(data[0].isAccurate); //-->true
    out(data[0].isAcquirer); //-->true
    out(data[0].isIssuer); //-->false
    out(data[0].isOpen); //-->true
    out(data[0].issuerId); //-->5258
    out(data[0].lastModifiedBy); //-->user1234
    out(data[0].lastModifiedDate); //-->2017-11-10T13:01:30
    out(data[0].merchantId); //-->0024038000200
    out(data[0].progressState); //-->CB1-4807-O-A-NEW
    out(data[0].claimType); //-->Standard
    out(data[0].claimValue); //-->25.00 USD
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
