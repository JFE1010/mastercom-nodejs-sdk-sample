const mastercom = require("mastercard-mastercom");

const MasterCardAPI = mastercom.MasterCardAPI;
const resourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

// One time MasterCardAPI initialization:
//
const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

const requestData = {
  caseType: "4",
  chargebackRefNum: ["1111423456", "2222123456"],
  customerFilingNumber: "5482",
  violationCode: "D.2",
  violationDate: "2017-11-13",
  disputeAmount: "200.00",
  currencyCode: "USD",
  fileAttachment: {
    filename: "test.tif",
    file: "sample file"
  },
  filedAgainstIca: "004321",
  filingAs: "A",
  filingIca: "001234",
  memo: "This is a test memo"
};

mastercom.CaseFiling.create(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.caseId);
  }
});

function out(value) {
  console.log("in out:", value);
}

function outObj(item, key) {
  console.log("in outObj:", item[key]);
}

function err(value) {
  console.error("in err:", value);
}
