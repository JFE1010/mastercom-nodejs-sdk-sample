const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = { "claim-id": "200002020654" };
mastercom.Fraud.getPossibleValueListsForCreate(requestData, function(
  error,
  data
) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.acctDeviceTypes[0].name); //-->1
    out(data.acctDeviceTypes[0].value); //-->1 - Chip with PIN
    out(data.acctStatuses[0].name); //-->N
    out(data.acctStatuses[0].value); //-->N - Account has not been closed
    out(data.cardValidCodes[0].name); //-->M
    out(data.cardValidCodes[0].value); //-->M - CVC 2 Valid
    out(data.subTypes[0].name); //-->01
    out(data.subTypes[0].value); //-->01 - Card reported stolen
    //This sample shows looping through acctDeviceTypes
    console.log("This sample shows looping through acctDeviceTypes");
    data.acctDeviceTypes.forEach(function(item) {
      outObj(item, "name");
      outObj(item, "value");
    });
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
