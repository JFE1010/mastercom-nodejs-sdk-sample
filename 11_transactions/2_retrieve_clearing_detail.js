const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  "claim-id": "200002020654",
  "transaction-id":
    "FIEaEgnM3bwPijwZgjc3Te+Y0ieLbN9ijUugqNSvJmVbO1xs6Jh5iIlmpOpkbax79L8Yj1rBOWBACx+Vj17rzvOepWobpgWNJNdsgHB4ag"
};
mastercom.Transactions.retrieveClearingDetail("", requestData, function(
  error,
  data
) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.accountLevelManagementAccountCategoryCode); //-->N
    out(data.acquirerReferenceData); //-->25131304365000000033393
    out(data.acquiringInstitutionIdCode); //-->999663
    out(data.approvalCode); //-->111111
    out(data.businessCycle); //-->01
    out(data.businessServiceArrangementTypeCode); //-->2
    out(data.businessServiceIdCode); //-->10001
    out(data.cardAcceptorBusinessCode); //-->5411
    out(data.cardAcceptorCity); //-->TEST CITY
    out(data.cardAcceptorClassificationOverrideIndicator); //-->N
    out(data.cardAcceptorCountry); //-->USA
    out(data.cardAcceptorIdCode); //-->Test ID
    out(data.cardAcceptorName); //-->TEST MERCHANT NAME
    out(data.cardAcceptorPostalCode); //-->TEST ZIP
    out(data.cardAcceptorState); //-->MO
    out(data.cardAcceptorStreetAddress); //-->TEST STREET ADDRESS
    out(data.cardAcceptorTerminalId); //-->73429189
    out(data.cardAcceptorUrl); //-->www.yoururl.com
    out(data.cardCaptureCapability); //-->9
    out(data.cardDataInputCapability); //-->5
    out(data.cardDataInputMode); //-->R
    out(data.cardDataOutputCapability); //-->0
    out(data.cardholderAuthenticationCapability); //-->9
    out(data.cardholderAuthenticationEntity); //-->9
    out(data.cardholderAuthenticationMethod); //-->9
    out(data.cardholderBillingAmount); //-->2500
    out(data.cardholderBillingCurrencyCode); //-->840
    out(data.cardholderFromAccountCode); //-->00
    out(data.cardholderPresentData); //-->0
    out(data.cardholderToAccountCode); //-->00
    out(data.cardIssuerReferenceData); //-->9000000959
    out(data.cardPresentData); //-->1
    out(data.cardProgramIdentifier); //-->MCC
    out(data.centralSiteBusinessDate); //-->171014
    out(data.centralSiteProcessingDateOriginalMessage); //-->060318
    out(data.currencyCodeReconciliation); //-->840
    out(data.currencyCodeTransaction); //-->840
    out(data.currencyExponentCardholderBilling); //-->2
    out(data.currencyExponentReconciliation); //-->2
    out(data.currencyExponentTransaction); //-->2
    out(data.dataRecord); //-->1
    out(data.electronicCommerceCardAuth); //-->0
    out(data.electronicCommerceSecurityLevelIndicator); //-->0
    out(data.electronicCommerceUcafCollectionIndicator); //-->2
    out(data.forwardingInstitutionIdCode); //-->5258
    out(data.functionCode); //-->200
    out(data.gcmsProductIndentifier); //-->MPL
    out(data.installmentPaymentData); //-->20
    out(data.installmentPaymentDataAnnualPercentageRate); //-->0
    out(data.installmentPaymentDataFirstInstallmentAmount); //-->24
    out(data.installmentPaymentDataInstallmentFee); //-->0
    out(data.installmentPaymentDataInterestRate); //-->23
    out(data.installmentPaymentDataNumberInstallments); //-->2
    out(data.installmentPaymentDataSubsequentInstallmentAmount); //-->20
    out(data.integratedCircuitCardRelatedData); //-->100
    out(data.interchangeRateDesignator); //-->79
    out(data.licensedProductIndentifier); //-->MPL
    out(data.legalCorporateName); //-->COMPANY ABC LLC
    out(data.localTransactionDateTime); //-->170719010100
    out(data.mastercardAssignedId); //-->PDS176
    out(data.mastercardAssignedIdOverrideIndicator); //-->N
    out(data.mastercardMappingServiceAccountNumber); //-->5154676300000001
    out(data.masterPassIncentiveIndicator); //-->N
    out(data.messageReasonCode); //-->1401
    out(data.messageReversalIndicator); //-->R
    out(data.originatingMessageFormat); //-->2
    out(data.partnerIdCode); //-->PDS190
    out(data.pinCaptureCapability); //-->1
    out(data.primaryAccountNumber); //-->5154676300000001
    out(data.processingCode); //-->00
    out(data.productOverrideIndicator); //-->Y
    out(data.programRegistrationId); //-->C57
    out(data.qpsPaypassEligibilityIndicator); //-->I
    out(data.rateIndicator); //-->N
    out(data.receivingInstitutionIdCode); //-->2202
    out(data.reconciliationAmount); //-->2500
    out(data.reconciliationCurrencyCode); //-->840
    out(data.remotePaymentsProgramData); //-->1
    out(data.serviceCode); //-->200
    out(data.settlementData); //-->1
    out(data.settlementIndicator); //-->M
    out(data.specialConditionsIndicator); //-->N
    out(data.terminalDataOutputCapability); //-->0
    out(data.terminalOperatingEnvironment); //-->2
    out(data.terminalType); //-->CT6
    out(data.tokenRequestorId); //-->vwxyz123456
    out(data.transactionAmountLocal); //-->2500
    out(data.transactionCategoryIndicator); //-->02
    out(data.transactionCurrencyCode); //-->840
    out(data.transactionDestinationInstitutionIdCode); //-->2705
    out(data.transactionLifeCycleId); //-->MPL2OSTCV0728
    out(data.transactionOriginatorInstitutionIdCode); //-->5258
    out(data.transactionType); //-->Clearing
    out(data.transitProgramCode); //-->05
    out(data.walletIdentifierMdes); //-->101
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
