const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  "claim-id": "200002020654",
  "transaction-id":
    "FIEaEgnM3bwPijwZgjc3Te+Y0ieLbN9ijUugqNSvJmVbO1xs6Jh5iIlmpOpkbax79L8Yj1rBOWBACx+Vj17rzvOepWobpgWNJNdsgHB4ag"
};
mastercom.Transactions.retrieveAuthorizationDetail("", requestData, function(
  error,
  data
) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.accountNumber); //-->5154676300000001
    out(data.accountNumberIndicator); //-->I
    out(data.acquirer); //-->N
    out(data.acquiringInstitutionCountryCode); //-->USA
    out(data.acquiringInstitutionId); //-->2705
    out(data.addressVerificationServiceResponse); //-->S
    out(data.adviceReasonCode); //-->160
    out(data.atcDiscrepancyIndicator); //-->G
    out(data.atcDiscrepancyValue); //-->00005
    out(data.atcValue); //-->00053
    out(data.authenticationIndicator); //-->1
    out(data.authorizationIdResponse); //-->11111
    out(data.banknetDate); //-->170719
    out(data.banknetReferenceNumber); //-->SDYP1MRWD
    out(data.billingCurrencyCode); //-->840
    out(data.cardAcceptorCity); //-->SAINT LOUIS
    out(data.cardAcceptorId); //-->0024038000200
    out(data.cardAcceptorName); //-->Test Name
    out(data.cardAcceptorState); //-->MO
    out(data.cardAcceptorTerminalId); //-->TERM-041
    out(data.cardholderActivatedTerminalLevel); //-->6
    out(data.cardholderBillingActualAmount); //-->000000010000
    out(data.cardholderBillingAmount); //-->000000010000
    out(data.cardAuthenticationMethodValidationCode); //-->N
    out(data.conversionDate); //-->0727
    out(data.conversionRate); //-->61000000
    out(data.electronicCommerceIndicators); //-->215
    out(
      data.electronicCommerceSecurityLevelIndicatorAndUcafCollectionIndicator
    ); //-->10
    out(data.expirationDatePresenceInd); //-->N
    out(data.finalAuthorizationIndicator); //-->0
    out(data.financialNetworkCode); //-->MPL
    out(data.forwardingInstitutionId); //-->5258
    out(data.infData); //-->4814653169024340
    out(data.integratedCircuitCardRelatedData); //-->100
    out(data.issuer); //-->N
    out(data.mastercardPromotionCode); //-->HGMINS
    out(data.mccMessageId); //-->MCW
    out(data.merchantAdviceCode); //-->03
    out(data.merchantCategoryCode); //-->3370
    out(data.originalAcquiringInstitutionIdCode); //-->2705
    out(
      data.originalElectronicCommerceSecurityLevelIndicatorAndUcafCollectionIndicator
    ); //-->0
    out(data.originalIssuerForwardingInstitutionIdCode); //-->5258
    out(data.originalMessageTypeIdentifier); //-->0110
    out(data.pinServiceCode); //-->TV
    out(data.posCardDataTerminalInputCapability); //-->0
    out(data.posCardholderPresence); //-->0
    out(data.posCardPresence); //-->0
    out(data.posEntryModePan); //-->05
    out(data.posEntryModePin); //-->1
    out(data.posTerminalAttendance); //-->0
    out(data.posTerminalLocation); //-->0
    out(data.posTransactionStatus); //-->0
    out(data.primaryAccountNumber); //-->510001000000134
    out(data.primaryAccountNumberAccountRange); //-->888888888
    out(data.privateData); //-->38038405002UU90220107ACQREG10207ISSREG17104C2C 102101920CM04020CM0402S1I13530411000000000000501006040CVA07040CXL00031440101R0201A4011SN402RG123A42430106GLBALL02050000103021704020105021606020041280110019 020 03100485604875
    out(data.processingCode); //-->00
    out(data.realTimeSubstantiationIndicator); //-->0
    out(data.reasonForUcafCollectionIndicatorDowngrade); //-->210
    out(data.recordDataPresenceIndicator); //-->N
    out(data.responseCode); //-->00
    out(data.retrievalReferenceNumber); //-->730607628081
    out(data.settlementActualAmount); //-->000000010000
    out(data.settlementDate); //-->0728
    out(data.stan); //-->002511
    out(data.storageTechnology); //-->01
    out(data.systemsTraceAuditNumber); //-->002511
    out(data.tokenAssuranceLevel); //-->99
    out(data.tokenRequestorId); //-->12345678936
    out(data.track1); //-->N
    out(data.track2); //-->Y101
    out(data.transactionActualAmount); //-->000000010000
    out(data.transactionAmountLocal); //-->10000
    out(data.transactionCategoryCode); //-->R
    out(data.transactionCurrencyCode); //-->840
    out(data.transactionType); //-->Authorization
    out(data.transmissionDateAndTime); //-->728075837
    out(data.universalCardholderAuthenticationFieldUcaf); //-->PARTIALSHIPMENT0000000000000 ALrP9TrnbuMCAANkrglrAoABFA&#x3D;&#x3D; ACFa0knOekU7AAnwugwJAoABFA&#x3D;&#x3D; ICQk7mTHQKqwx9tKqqY&#x3D; hgeiVCYsZLM8YwAAAFcqCVkAAAA&#x3D; hgeiVCYsZLM8YwAAAIFuCEYAAAA&#x3D; hmJA2XBYTaCdCAEAABneAAAAAAA&#x3D;
    out(data.vcnProductCode); //-->MCO
    out(data.walletIdentifier); //-->100
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
