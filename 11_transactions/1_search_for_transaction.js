const mastercom = require("mastercard-mastercom");
const MasterCardAPI = mastercom.MasterCardAPI;
const ResourceConfig = mastercom.ResourceConfig;

const consumerKey =
  "4adxfo6dRbw-vDo_zSireresVv6pEzoBivnobpfac62587cf!bc7b5889bb1b424cb29ce74b20c0b9410000000000000000";
const keyStorePath =
  "../../Sandbox Signing Key/Jonathan's_Big_Project-sandbox.p12";
const keyAlias = "keyalias";
const keyPassword = "keystorepassword";

const authentication = new MasterCardAPI.OAuth(
  consumerKey,
  keyStorePath,
  keyAlias,
  keyPassword
);
MasterCardAPI.init({
  sandbox: true,
  debug: true,
  authentication: authentication
});
MasterCardAPI.setEnvironment("sandbox");

// insert requestData and process here:
const requestData = {
  acquirerRefNumber: "05436847276000293995738",
  primaryAccountNum: "5488888888887192",
  transAmountFrom: "10000",
  transAmountTo: "20050",
  tranStartDate: "2018-10-01",
  tranEndDate: "2018-10-01"
};
mastercom.Transactions.searchForTransaction(requestData, function(error, data) {
  if (error) {
    err("HttpStatus: " + error.getHttpStatus());
    err("Message: " + error.getMessage());
    err("ReasonCode: " + error.getReasonCode());
    err("Source: " + error.getSource());
    err(error);
  } else {
    out(data.authorizationSummaryCount); //-->1
    out(data.message); //-->Search returned 1 records
    out(data.authorizationSummary[0].originalMessageTypeIdentifier); //-->0110
    out(data.authorizationSummary[0].banknetDate); //-->160107
    out(data.authorizationSummary[0].transactionAmountUsd); //-->401.17
    out(data.authorizationSummary[0].primaryAccountNumber); //-->5488888888887192
    out(data.authorizationSummary[0].processingCode); //-->00
    out(data.authorizationSummary[0].transactionAmountLocal); //-->000000010000
    out(data.authorizationSummary[0].authorizationDateAndTime); //-->108125633
    out(data.authorizationSummary[0].track2); //-->Y101
    out(data.authorizationSummary[0].authenticationId); //-->111111
    out(data.authorizationSummary[0].cardAcceptorName); //-->MASTERCARD
    out(data.authorizationSummary[0].cardAcceptorCity); //-->SAINT LOUIS
    out(data.authorizationSummary[0].cardAcceptorState); //-->MO
    out(data.authorizationSummary[0].track1); //-->N
    out(data.authorizationSummary[0].currencyCode); //-->840
    out(data.authorizationSummary[0].chipPresent); //-->N
    out(data.authorizationSummary[0].transactionId); //-->hqCnaMDqmto4wnL+BSUKSdzROqGJ7YELoKhEvluycwKNg3XTzSfaIJhFDkl9hW081B5tTqFFiAwCpcocPdB2My4t7DtSTk63VXDl1CySA8Y
    out(data.authorizationSummary[0].clearingSummary[0].primaryAccountNumber); //-->5488888888887192
    out(data.authorizationSummary[0].clearingSummary[0].transactionAmountLocal); //-->2500
    out(data.authorizationSummary[0].clearingSummary[0].dateAndTimeLocal); //-->170719010100
    out(
      data.authorizationSummary[0].clearingSummary[0]
        .cardholderAuthenticationCapability
    ); //-->9
    out(data.authorizationSummary[0].clearingSummary[0].cardPresent); //-->1
    out(
      data.authorizationSummary[0].clearingSummary[0].acquirerReferenceNumber
    ); //-->05413364365000000000667
    out(data.authorizationSummary[0].clearingSummary[0].cardAcceptorName); //-->TEST MERCHANT NAME
    out(data.authorizationSummary[0].clearingSummary[0].currencyCode); //-->840
    out(data.authorizationSummary[0].clearingSummary[0].transactionId); //-->U7dImb1ncs24LKNU9dDpl+FHlPzyfYOOvS5PijTlO6wHH09l7aiVJ1KJHp3sWPUHH0l90J1U82DGrE3hq72A
    //This sample shows looping through authorizationSummary
    console.log("This sample shows looping through authorizationSummary");
    data.authorizationSummary.forEach(function(item) {
      outObj(item, "originalMessageTypeIdentifier");
      outObj(item, "banknetDate");
      outObj(item, "transactionAmountUsd");
      outObj(item, "primaryAccountNumber");
      outObj(item, "processingCode");
      outObj(item, "transactionAmountLocal");
      outObj(item, "authorizationDateAndTime");
      outObj(item, "track2");
      outObj(item, "authenticationId");
      outObj(item, "cardAcceptorName");
      outObj(item, "cardAcceptorCity");
      outObj(item, "cardAcceptorState");
      outObj(item, "track1");
      outObj(item, "currencyCode");
      outObj(item, "chipPresent");
      outObj(item, "transactionId");
      outObj(item, "clearingSummary");
    });
  }
});

function out(value) {
  console.log(value);
}

function outObj(item, key) {
  console.log(item[key]);
}

function err(value) {
  console.error(value);
}
