#CLI app for MasterCom in Node
This is a command line based app that runs all the API calls for Mastercom v4 ([documentation found here](https://developer.mastercard.com/documentation/mastercom/4)).

##Use
You will need NodeJs installed on your computer. Once that has been done, and the app is downloaded, from the command line in the top level directory (where you have your package.json file), run the command `npm i` to install all necessary dependencies. You app will now be ready to use. Each file is run individually and independently of the others using the following syntax:

```
    $ node [filename]

    e.g.:
    $ node 1_create.js
```
